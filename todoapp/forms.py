from django import forms
from django.contrib.auth.models import User
from .models import TodoList, Task


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'password']


class TaskForm(forms.ModelForm):
    to_be_completed = forms.DateField(widget=forms.SelectDateWidget())

    class Meta:
        model = Task
        fields = ['task_name', 'description', 'to_be_completed']


class ListForm(forms.ModelForm):

    class Meta:
        model = TodoList
        fields = ['list_name']
