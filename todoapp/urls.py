from django.conf.urls import url
from django.contrib.auth.views import login, logout
from . import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^login/$', login, {'template_name': 'auth/login.html'}, name='login'),
    url(r'^logout/$', logout, {'next_page': '/'}, name='logout'),
    url(r'^add_list/$', views.add_list, name='add_list'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^(?P<list_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^(?P<list_id>[0-9]+)/(?P<task_id>[0-9]+)/complete/$', views.complete, name='complete'),
    url(r'^(?P<list_id>[0-9]+)/(?P<task_id>[0-9]+)/delete/$', views.delete_task, name='delete_task'),
    url(r'^(?P<list_id>[0-9]+)/add_task/$', views.add_task, name='add_task'),
]