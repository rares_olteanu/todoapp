from django import template
from datetime import date

register = template.Library()


@register.filter(name='get_due_date_string')
def get_due_date_string(value):
    delta = value - date.today()

    if delta.days < 0:
        return "deadline missed by %s days" % abs(delta.days)
    elif delta.days == 0:
        return "Due: Today!"
    elif delta.days == 1:
        return "Due: Tomorrow"
    elif delta.days > 1:
        return "Due: In %s days" % delta.days

