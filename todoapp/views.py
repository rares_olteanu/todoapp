from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate
from django.urls import reverse
from django.utils import timezone
from django.views import generic
from .models import Task, TodoList

from .forms import UserForm, TaskForm, ListForm


class IndexView(generic.ListView):
    template_name = 'todoapp/index.html'
    context_object_name = 'lists'
    paginate_by = 10

    def get_queryset(self):
        if not self.request.user.is_anonymous():
            return TodoList.objects.filter(user=self.request.user).order_by('created')
        else:
            return []


def detail(request, list_id):
    mylist = get_object_or_404(TodoList, pk=list_id)
    mytasks = Task.objects.filter(todo_list=mylist).order_by('created')
    return render(request, 'todoapp/detail.html', {'mylist': mylist, 'tasks': mytasks})


@login_required
def complete(request, list_id, task_id):
    mylist = get_object_or_404(TodoList, pk=list_id)
    mytask = get_object_or_404(Task, pk=task_id)
    mytask.completed = True
    mytask.save()
    mytasks = Task.objects.filter(todo_list=mylist)
    return render(request, 'todoapp/detail.html',  {'mylist': mylist, 'tasks': mytasks})


@login_required
def delete_task(request, list_id, task_id):
    mylist = get_object_or_404(TodoList, pk=list_id)
    task = get_object_or_404(Task, pk=task_id)
    task.delete()
    mytasks = Task.objects.filter(todo_list=mylist)
    return HttpResponseRedirect(reverse('todoapp:detail', args=(list_id,)))


@login_required
def add_task(request, list_id):
    form = TaskForm(request.POST or None)
    if form.is_valid():
        mylist = get_object_or_404(TodoList, pk=list_id)
        task = form.save(commit=False)
        task.todo_list = mylist
        task.task_name = form.cleaned_data['task_name']
        task.description = form.cleaned_data['description']
        task.to_be_completed = form.cleaned_data['to_be_completed']
        task.created = timezone.now()
        task.completed = False
        task.save()
        return HttpResponseRedirect(reverse('todoapp:detail', args=(list_id,)))
    return render(request, 'todoapp/add_task.html', {'form': form})


@login_required
def add_list(request):
    form = ListForm(request.POST or None)
    if form.is_valid():
        mylist = form.save(commit=False)
        mylist.user = request.user
        mylist.list_name = form.cleaned_data['list_name']
        mylist.created = timezone.now()
        mylist.save()
        return redirect('/')
    return render(request, 'todoapp/add_task.html', {'form': form})


def signup(request):
    form = UserForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user.set_password(password)
        user.save()
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('/')

    form = UserForm()
    return render(request, 'todoapp/signup.html', {'form': form})
