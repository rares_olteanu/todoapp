from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


class TodoList(models.Model):
    list_name = models.CharField(max_length=200)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField(default=timezone.now())

    def __str__(self):
        return self.list_name


class Task(models.Model):
    task_name = models.CharField(max_length=200)
    description = models.CharField(max_length=500)
    created = models.DateTimeField(default=timezone.now())
    to_be_completed = models.DateField()
    completed = models.BooleanField(default=False)
    todo_list = models.ForeignKey(TodoList, on_delete=models.CASCADE)

    def __str__(self):
        return self.task_name
