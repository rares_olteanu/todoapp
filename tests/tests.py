import pytest
from django.utils import timezone
from todoapp.models import TodoList, Task
from django.http import HttpResponse


@pytest.fixture
def client(request, client):
    func = client.request

    def wrapper(**kwargs):
        print('>>>>', ' '.join('{}={!r}'.format(*item)
                               for item in kwargs.items()))
        resp = func(**kwargs)
        print('<<<<', resp, resp.content)
        return resp
    client.request = wrapper
    return client


@pytest.fixture
def authed_client(db, client, admin_user):
    client.force_login(admin_user)
    return client


@pytest.fixture
def lists(request, db, admin_user):
    return TodoList.objects.bulk_create([
        TodoList(list_name='todo1', created=timezone.now(), user=admin_user),
        TodoList(list_name='todo2', created=timezone.now() - timezone.timedelta(days=1), user=admin_user),
        TodoList(list_name='todo3', created=timezone.now() - timezone.timedelta(days=2), user=admin_user),
    ])


@pytest.fixture
def tasks(request, db, lists):
    return Task.objects.bulk_create([
        Task(task_name='foo', description='foofoo', created=timezone.now(),
             to_be_completed=timezone.now() + timezone.timedelta(days=1), completed=False, todo_list=lists[0]),
        Task(task_name='bar', description='foobar', created=timezone.now() - timezone.timedelta(days=1),
             to_be_completed=timezone.now() + timezone.timedelta(days=2), completed=False, todo_list=lists[0]),
        Task(task_name='abc', description='abcd', created=timezone.now() - timezone.timedelta(days=2),
             to_be_completed=timezone.now() + timezone.timedelta(days=3), completed=False, todo_list=lists[0]),
    ])


def test_index(lists, authed_client):
    resp = authed_client.get('/')
    assert resp.status_code == 200
    assert list(resp.context['lists']) == sorted(lists, key=lambda x: x.created)
    content = resp.content.decode(resp.charset)
    for lst in lists:
        assert lst.list_name in content


def test_index_not_logged(lists, client):
    resp = client.get('/')
    assert resp.status_code == 200
    assert list(resp.context['lists']) == []


def test_index_empty(db, authed_client):
    resp = authed_client.get('/')
    assert resp.status_code == 200
    assert len(resp.context['lists']) == 0
    content = resp.content.decode(resp.charset)
    assert 'No tasks' in content


def test_detail(lists, tasks, authed_client):
    mylist = TodoList.objects.get(list_name='todo1')
    resp = authed_client.get('/%s/' % mylist.id)
    assert resp.status_code == 200
    assert resp.context['mylist'] == lists[0]
    assert list(resp.context['tasks']) == sorted(tasks, key=lambda x: x.created)
    content = resp.content.decode(resp.charset)
    assert 'foofoo' in content


def test_add_task(db, lists, authed_client):
    resp = authed_client.post('/%s/add_task/' % lists[0].id, {'task_name': 'Foo',
                              'description': 'foo',
                              'created': timezone.now(),
                              'to_be_completed': timezone.now().date(),
                              'completed': False,
                              'todo_list': lists[0]})
    assert resp.status_code == 302
    assert resp.url == '/%s/' % lists[0].id
